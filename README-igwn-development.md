# IGWN Development API/ABI Testing Yum Repository

The IGWN Development API/ABI Testing Yum Repository contains RPMS for testing
packages undergoing API/ABI transitions produced by IGWN member groups.

See <https://computing.docs.ligo.org/guide/software/> for details on
IGWN Software practices.
