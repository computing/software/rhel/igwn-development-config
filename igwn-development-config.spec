Name:      igwn-development-config
Version:   8
Release:   3%{?dist}
Summary:   Yum/DNF configuration for IGWN Development API/ABI Testing Repository

License:   GPLv3+
BuildArch: noarch
Requires:  redhat-release >= %{version}
Url: https://software.igwn.org/lscsoft/rocky/%{version}/development/

Source0:   igwn-development.repo
Source1:   COPYING
Source2:   README-igwn-development.md

%description
Yum/DNF configuation for IGWN Development API/ABI Testing Repository on Rocky
Linux %{version}

%prep
%setup -q -c -T
install -pm 644 %{SOURCE1} .
install -pm 644 %{SOURCE2} .

%build

%install
install -dm 755 %{buildroot}%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc README-igwn-development.md
%license COPYING
%config(noreplace) %{_sysconfdir}/yum.repos.d/igwn-development.repo


# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Wed Feb 01 2023 Adam Mercer <adam.mercer@ligo.org> 8-3
- disable source repository by default

* Wed Feb 01 2023 Adam Mercer <adam.mercer@ligo.org> 8-2
- add source repository

* Wed Jan 04 2023 Adam Mercer <adam.mercer@ligo.org> 8-1
- initial version for Rocky Linux 8
